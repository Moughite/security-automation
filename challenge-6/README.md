# Challenge 6

This challenge describes how to run our SAST (Semgrep) scan

----


### Pull the Bandit container

##### command:

```bash
docker pull returntocorp/semgrep
```

#### output:

```
Using default tag: latest
latest: Pulling from returntocorp/semgrep
cbdbe7a5bc2a: Downloading [=================>                                 ]    961kB/2.813MB
26ebcd19a4e3: Download complete 
8341bd19193b: Downloading [==>                                                ]  846.3kB/20.69MB
ecc595bd65e1: Waiting 
4b1c9d8f69d2: Waiting 
4acb96206c62: Waiting 
88da57106cb7: Waiting 
......... snip ........
```

----

### Change your directory to the flask-app dir

##### command:

```bash
cd flask-app
```
--

### Start our semgrep scanner

##### command:

Note that $(pwd) is only supported on Linux and MacOS - on Windows you will need to replace this with the full current working directory.

```bash
docker run --rm -v "${PWD}:/src" returntocorp/semgrep --config "https://semgrep.dev/p/bandit"
```

##### output:

```bash
using config from https://semgrep.dev/p/bandit. Visit https://semgrep.dev/registry to see all public rules.
downloading config...
running 60 rules.
```
----

### Start our semgrep scanner

##### command:

Note that $(pwd) is only supported on Linux and MacOS - on Windows you will need to replace this with the full current working directory.

```bash
docker run --rm -v "${PWD}:/src" returntocorp/semgrep --config "https://semgrep.dev/p/flask"
```

##### output:

```bash
using config from https://semgrep.dev/p/flask. Visit https://semgrep.dev/registry to see all public rules.
downloading config...
running 18 rules...
LFI.py
severity:warning rule:python.flask.security.audit.app-run-param-config.avoid_app_run_with_bad_host: Running flask app with host 0.0.0.0 could expose the server publicly.
23:    app.run(host='0.0.0.0')
severity:error rule:python.flask.security.injection.path-traversal-open.path-traversal-open: Found request data in a call to 'open'. Ensure the request data is validated or sanitized, otherwise it could result in path traversal attacks.
18:    f = open(filename,'r')
```
----